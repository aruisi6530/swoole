<?php
/************************************************************
 * Copyright (C), 1993-~, Dacelve. Tech., Ltd.
 * FileName : Redis.php
 * Author   : Lizhijian
 * Version  : 1.0
 * Date     : 2018/2/7 17:18
 * Description   : 阿里云Redis 应用类
 * Main Function :
 * History  :
 * <author>    <time>    <version >    <desc>
 * Lizhijian   2018/2/8   1.0          init
 ***********************************************************/
namespace app;
/**
 * redis应用类
 * Class Redis
 * @package app\common\library
 * Fashion:
 */
class Redis
{

    /**
     * @var \Redis
     */
    protected static $redis;
    public static $dbindex = 0;//操作的数据库下标

    /**
     * Redis constructor.
     */
    public function __construct()
    {
        return self::init();
    }

    /**
     * @name redis链接
     * @Description
     * @remark
     * @author Lizhijian
     * @return \Redis
     * @example
     */
    public static function init()
    {
        self::$redis = new \Redis;
        self::$redis->connect('120.79.209.186', 6379);
        self::$redis->auth('myRedis123');
        self::$redis->select(self::$dbindex);
        return self::$redis;
    }

    /**
     * 判断是否JSON格式
     * @Description
     * @param $string
     * @return bool
     * @example
     * @author Lizhijian
     * @since 2018/6/19 14:00
     */
    protected static function is_json($string) {
        $type = is_object(json_decode($string));

        if($string[0] == '[' && $string[strlen($string)-1] == ']'){
            $type = 1;
        }
        switch ($type){
            case 1;
                return true;
                break;
            default;
                return false;
                break;
        }
    }

    /**
     * 设置数据库
     * @Description
     * @param int $dbindex 数据库名0,1,2...
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:16
     */
    public static function setDb($dbindex = 0){
        self::$dbindex = $dbindex;
    }

    /**
     * 设置队列数据库
     * @Description
     * @param int $dbindex 数据库名0,1,2...
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:20
     */
    public static function setQDb($dbindex = 1){
        self::$Qdbindex = $dbindex;
    }

    /**
     * 设置链表数据库
     * @Description
     * @param int $dbindex 数据库名0,1,2...
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:21
     */
    public static function setLDb($dbindex = 2){
        self::$Ldbindex = $dbindex;
    }

    /**
     * 设置集合数据库
     * @Description
     * @param int $dbindex 数据库名0,1,2...
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:24
     */
    public static function setSDb($dbindex = 2){
        self::$Sdbindex = $dbindex;
    }

    /**
     * 设置指定键的值
     * @Description
     * @param $key
     * @param $val
     * @param int $time
     * @example
     * @author Lizhijian
     * @since 2018/6/19 13:59
     */
    public static function set($key, $val, $time = 0)
    {
        self::init();
        if (is_array($val)) {
            $val = json_encode($val);
        }
        $time > 0 ? self::$redis->setex($key, $time, $val) : self::$redis->set($key, $val);
    }

    /**
     * 设置指定键的值（若值存在则不设置）
     * @Description
     * @param $key
     * @param $val
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:56
     */
    public static function setnx($key, $val){
        self::init();
        self::$redis->setnx($key, $val);
    }

    /**
     * 获取指定键的值
     * @Description
     * @param $key
     * @param string $params
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 14:00
     */
    public static function get($key)
    {
        self::init();
        $res = self::$redis->get($key);
        return self::is_json($res) ? json_decode($res, true) : $res;
    }

    /**
     * 删除
     * @Description
     * @param $key
     * @param string $delCount
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:10
     */
    public static function del($key)
    {
        self::init();
        return self::$redis->del($key);
    }

    /**
     * 是否存在键
     * @Description
     * @param $key
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 17:01
     */
    public function exists($key){
        self::init();
        return self::$redis->exists($key);
    }

    /**
     * 数字递增存储键值键
     * @Description
     * @param $key
     * @return mixed
     * @example set('test',"123"), incr("test")-结果124
     * @author Lizhijian
     * @since 2018/6/19 17:05
     */
    public function incr($key){
        self::init();
        return self::$redis->incr($key);
    }

    /**
     * 数字递减存储键值键
     * @Description
     * @param $key
     * @return mixed
     * @example set('test',"123"), incr("test")-结果122
     * @author Lizhijian
     * @since 2018/6/19 17:06
     */
    public function decr($key){
        self::init();
        return self::$redis->decr($key);
    }

    /**
     * 获取多个键的值
     * @Description
     * @param array $key
     * @return bool
     * @example getMultiple(array('test1','test2')，结果：Array ( [0] => 1 [1] => 2 )
     * @author Lizhijian
     * @since 2018/6/19 17:07
     */
    public function getMultiple($key = array()){
        if(!is_array($key)){
            return false;
        }
        self::init();
        return self::$redis->getMultiple($key);
    }

    /**
     * 搜索值
     * @Description
     * @param $key
     * @return mixed
     * @example keys('test'), keys('tes*'), keys('*')
     * @author Lizhijian
     * @since 2018/6/19 16:11
     */
    public static function keys($key)
    {
        self::init();
        return self::$redis->keys($key);
    }

    /**
     * 队列 从头部入队
     * @Description
     * @param $key
     * @param $value1
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:13
     */
    public static function lPush($key, $value1)
    {
        self::init();
        self::$redis->select(self::$Qdbindex);
        if(is_array($value1)){
            $value1 = json_encode($value1);
        }
        return self::$redis->lPush($key, $value1);
    }

    /**
     * 队列 从尾部入队
     * @Description
     * @param $key
     * @param $value1
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:13
     */
    public static function rPush($key, $value1)
    {
        self::init();
        self::$redis->select(self::$Qdbindex);
        return self::$redis->rPush($key, $value1);
    }

    /**
     * 队列 从尾部出队
     * @Description
     * @param $key
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:22
     */
    public static function rPop($key)
    {
        self::init();
        self::$redis->select(self::$Qdbindex);

        $res = self::$redis->rPop($key);

        if(is_json($res)){
            $res = json_decode($res, true);
        }
        return $res;
    }

    /**
     * 队列 从头部出队
     * @Description
     * @param $key
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:22
     */
    public static function lPop($key)
    {
        self::init();
        self::$redis->select(self::$Qdbindex);
        $res = self::$redis->lPop($key);
        if(is_json($res)){
            $res = json_decode($res, true);
        }
        return $res;
    }

    /**
     * 获取队列长度、尺寸
     * @Description
     * @param $key
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 17:09
     */
    public static function lLen($key){
        self::init();
        return self::$redis->lLen($key);
    }

    /**
     * 获取队列指定下标的值
     * @Description
     * @param $key
     * @return mixed
     * @example lget("test",3)),结果：string(3) "444"
     * @author Lizhijian
     * @since 2018/6/19 17:10
     */
    public static function lget($key){
        self::init();
        return self::$redis->lget($key);
    }

    /**
     * 为列表指定的索引赋新的值,若不存在该索引返回false.
     * @Description
     * @param $key
     * @param $index
     * @param $val
     * @return mixed
     * @example lset("test",1,"333"));  //结果：bool(true)
     * @author Lizhijian
     * @since 2018/6/19 17:27
     */
    public static function lset($key, $index, $val){
        self::init();
        return self::$redis->lset($key, $index, $val);
    }

    /**
     * 返回在该区域中的指定键列表中开始到结束存储的指定元素，lGetRange(key, start, end)。0第一个元素，1第二个元素… -1最后一个元素，-2的倒数第二…
     * @Description
     * @param $key
     * @param $start
     * @param $end
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 17:29
     */
    public static function lgetrange($key, $start, $end){
        self::init();
        return self::$redis->lgetrange($key, $start, $end);
    }

    /**
     * 从列表中从头部开始移除count个匹配的值。如果count为零，所有匹配的元素都被删除。如果count是负数，内容从尾部开始删除。
     * @Description
     * @param $key
     * @param $start
     * @param $end
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 17:29
     */
    public static function lremove($key, $start, $end){
        self::init();
        return self::$redis->lremove($key, $start, $end);
    }

    /**
     * 检查集合中是否存在指定的值。
     * @Description
     * @param $key
     * @param $val
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 17:31
     */
    public static function scontains($key, $val){
        self::init();
        return self::$redis->scontains($key, $val);
    }

    /**
     * 返回集合中存储值的数量
     * @Description
     * @param $key
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 17:32
     */
    public static function ssize($key){
        self::init();
        return self::$redis->ssize($key);
    }

    /**
     * 增加 集合
     * @Description
     * @param $key
     * @param $value1
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:24
     */
    public static function sAdd($key, $value1)
    {
        self::init();
        self::$redis->select(self::$Sdbindex);
        if(is_array($value1)){
            $value1 = json_encode($value1);
        }
        return self::$redis->sAdd($key, $value1);
    }

    /**
     * 移除集合元素
     * @Description
     * @param $key
     * @param $member1
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:25
     */
    public static function sRem($key, $member1)
    {
        self::init();
        self::$redis->select(self::$Sdbindex);
        return self::$redis->sRem($key, $member1);
    }

    /**
     * 移除有序集合元素
     * @Description
     * @param $key
     * @param $member1
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:25
     */
    public static function zRem($key, $member1)
    {
        self::init();
        self::$redis->select(self::$Sdbindex);
        return self::$redis->zRem($key, $member1);
    }

    /**
     * 获取集合所有成员
     * @Description
     * @param $key
     * @param bool $cantact
     * @return array
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:28
     */
    public static function sMembers($key, $cantact = false)
    {
        self::init();
        self::$redis->select(1);

        $res = self::$redis->sMembers($key);

        $back = array();
        foreach ($res as $k => $v){
            if(is_json($v)){
                if($cantact === true){
                    $back = array_merge(json_decode($v, true), $back);
                }else{
                    $back[$k] = json_decode($v, true);
                }
            }
        }
        return $back;
    }

    /**
     * 移除集合中的元素
     * @Description
     * @param $key
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:29
     */
    public static function sPop($key){
        self::init();
        self::$redis->sPop($key);
    }

    /**
     * 设置哈希表内容
     * @Description
     * @param $key
     * @param $hashKey
     * @param $value
     * @param int $time 哈希表的全局时间[设置单个key时注意]
     * @example hashSet('user', 'info', array('name' => 'Joe', 'salary' => 8000));
     * @author Lizhijian
     * @since 2018/6/19 16:31
     */
    public static function hashSet($key, $hashKey, $value, $time = 0){
        self::init();
        if(is_array($value)){
            $value = json_encode($value);
        }
        self::$redis->hSet($key, $hashKey, $value);
        if($time > 0) self::$redis->expire($key, $time);
    }

    /**
     * 获取哈希表内容
     * @Description
     * @param $key
     * @param string $hashKeys 无hashKeys时取所有域的值
     * @return mixed
     * @example  hashGet('user', 'info');  or  hashGet('user');
     * @author Lizhijian
     * @since 2018/6/19 16:29
     */
    public static function hashGet($key, $hashKeys = ''){
        self::init();
        if($hashKeys){
            $res = self::$redis->hGet($key, $hashKeys);
        }else{
            $res = self::$redis->hGetAll($key);
        }
        if(!$res){
            return [];
        }
        if(is_array($res)){
            foreach ($res as $k => $v){
                $res[$k] = self::is_json($v) ? json_decode($v, true) : $v;
            }
        }else{
            $res = json_decode($res, true);
        }
        return $res;
    }

    /**
     * 删除所有元素
     * @Description
     * @param $key
     * @param string $dbIndex
     * @return mixed
     * @example
     * @author Lizhijian
     * @since 2018/6/19 16:26
     */
    public static function deleteAll($key, $dbIndex = '')
    {
        self::init();
        $back = [];
        $dbIndex = (int)$dbIndex;
        if(!$dbIndex){
            for ($i=0;$i<50;$i++){
                self::$redis->select($i);
                $back[] = self::$redis->delete($key);
            }
        }else{
            self::$redis->select($dbIndex);
            $back[] = self::$redis->delete($key);
        }
        return $back;
    }
}